var App = (function (app, $) {
    "use strict";

    app.ui = {
        INFO: 'info',
        SUCCESS: 'success',
        WARNING: 'warning',
        DANGER: 'danger',
        ROYAL: 'royal',
        PRIMARY: 'primary',
        /**
         * Show alert
         * @param {Object} $container jquery object
         * @param {Array} messages
         * @param {String} type info|success|warning|danger|royal|primary
         */
        alert: function ($container, messages, type) {
            var template = '<div class="alert alert-border alert-{{type}}">' +
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                '</p>';
            var $alert = $(app.tools.render(template, {'type': type})).append(messages.join('<br>'));
            $container.find('.alert').remove();
            $container.prepend($alert);
        }
    };

    return app;
}(App || {}, $));


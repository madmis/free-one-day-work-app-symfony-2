var App = (function (app, $) {
    "use strict";

    app.task = {
        el: {
            form: $('#task-form'),
            country: $('#app_task_form_country'),
            region: $('#app_task_form_region'),
            city: $('#app_task_form_city')
        },
        formModel: null,
        init: function() {
            var self = this;
            $(function() {
                var $form = app.task.el.form;
                ko.applyBindings(app.task.formModel, $form.get(0));

                self.initCitySelect(app.task.el.city);
                self.initDropZone($form);
            });
        },
        initDropZone: function($form) {
            Dropzone.options.taskForm = {
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 5,
                maxFiles: 3,
                paramName: function(index) {
                    return 'app_task_form[files]['+ index +'][file]';
                },
                previewsContainer: "#previews",
                addRemoveLinks: true,
                acceptedFiles: 'image/*',
                url: $form.prop('action'),
                clickable: '.dropzone-previews',
                init: function() {
                    var myDropzone = this;
                    $(document).on('click', "button[type=submit]", function(event) {
                        //event.preventDefault();
                        //event.stopPropagation();
                        if (myDropzone.getQueuedFiles().length > 0) {
                            myDropzone.processQueue();
                        } else {
                            var data = $form.serialize();
                            $.post($form.prop('action'), data, function(response) {
                                window.location.href = response.data.taskUrl;
                            }).fail(function(jqXhr) {
                                app.ui.alert($form, jqXhr.responseJSON.error.messages, app.ui.DANGER);
                            });
                        }
                        return false;
                    });
                    this.on('error', function(file, response, xhr) {
                        var message = xhr ? response.error.messages : [response];
                        app.ui.alert($form, message, app.ui.DANGER);
                    });
                    this.on('successmultiple', function(files, response, xhr) {
                        window.location.href = response.data.taskUrl;
                    });
                }
            };
        },
        initCitySelect: function($element) {
            $element.select2({
                ajax: {
                    dataType: app.config.requestsDataType,
                    url: app.routing.buildUrl(app.routing.api.locations.city),
                    processResults: function (response) {
                        return { results: response.data };
                    }
                },
                escapeMarkup: function (markup) { return markup; },
                templateSelection: function (data) { return data.name; },
                templateResult: function(data) {
                    if (data.loading) { return data.text; }

                    return '<div class="clearfix">' +
                        '<div class="col-sm-10">' +
                        '<div class="clearfix">' +
                        '<div class="col-sm-6">' + data.name + ' <small>('+ data.country_name +' / ' + data.region_name + ')</small></div>' +
                        '</div></div></div>';
                },
                minimumInputLength: 2
            });

            $element.on("select2:select", function (e) {
                app.task.el.country.val(e.params.data.country_id);
                app.task.el.region.val(e.params.data.region_id);
            });
            $element.on("select2:unselect", function (e) {
                app.task.el.country.val('');
                app.task.el.region.val('');
            });
        },
        buildModel: function() {
            app.task.formModel = {
                title: ko.observable(),
                titleLabel: ko.observable(),
                description: ko.observable(),
                currency: ko.observable(),
                budget: ko.observable(),
                categories: ko.observableArray(),
                selected: ko.observable(0),
                maxSelected: ko.observable(2),

                currenciesList: ko.observableArray(),
                categoriesList: ko.observableArray(),
                selectedMaxCount: ko.pureComputed(function() {
                    return app.task.formModel.selected() >= app.task.formModel.maxSelected();
                })
            };

            var url = app.routing.buildUrl(app.routing.api.tasks.form_data, {});
            $.get(url).done(function( response ) {
                response.data.categories.forEach(function(item) {
                    app.task.formModel.categoriesList.push(new CategoriesItem(app.task.formModel, item));
                });
                app.task.formModel.currenciesList(response.data.currencies);
                app.task.formModel.maxSelected(response.data.maxSelectedCategories);
            }).fail(function( jaXHR ) {

            });
        }
    };

    var CategoriesItem = function(parentModel, category) {
        var self = this;
        $.extend(self, category);
        self.selected = ko.observable(0);

        category['__children'].forEach(function(item) {
            item.checked = ko.observable(0);

            item.checked.subscribe(function(value) {
                if (value) {
                    self.selected(self.selected() + 1);
                    parentModel.selected(parentModel.selected() + 1);
                } else {
                    self.selected(self.selected() - 1);
                    parentModel.selected(parentModel.selected() - 1);
                }
            }, item);
        });
    };

    app.task.buildModel();
    app.task.init();

    return app;
}(App || {}, $));
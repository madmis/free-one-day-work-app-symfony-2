var App = (function (app, $) {
    "use strict";

    app.search = {
        initSearch: function() {

            $( ".autocomplete" ).each(function( ) {
                var $self = $(this);

                console.debug($self);
                console.debug($self.closest('form').find('.results-container'));

                $self.autocomplete({
                    source: function( request, result ) {
                        var url = app.routing.buildUrl(app.routing.api.tasks.search, {'query': request.term});
                        $.get(url, function(response) {
                            result( $.map(response.data, function(item) {
                                item.label = item.title;
                                item.value = item.title;
                                return item
                            }));
                        }).fail(function(jqXHR, textStatus, errorThrown ) {
                        });
                    },
                    minLength: 3,
                    autoFocus: true,
                    appendTo: $self.closest('form').find('.results-container'),
                    select: function( event, ui ) {
                        app.routing.redirect(app.routing.html.tasks.view, {'slug': ui.item.slug});
                    },
                    open: function() {
                        $(".results-container > ul").css({left: 0, top: 0, width: 'inherit' });
                    }
                }).autocomplete( "instance" )._renderItem = function( ul, item ) {
                    return $( "<li>" )
                        .append( "<p><strong>" + item.label + "</strong><span class='pull-right label label-default'>" + item.currency + " " + item.budget + "</span></p>" )
                        .append( "<p>" + item.description.substring(0, 150) + "...</p>" )
                        .appendTo( ul );
                };
            });
        }
    };

    app.search.initSearch();

    return app;
}(App || {}, $));


var App = (function (app, $) {
    "use strict";

    app.routing = {
        /**
         * @param {String} route
         * @param {Object} params
         * @param {Boolean} isHtml
         */
        buildUrl: function(route, params, isHtml) {
            if (!isHtml) {
                route = '/api' + route;
            }

            return app.tools.render(route, params);
        },
        redirect: function(route, params) {
            window.location.href = app.routing.buildUrl(route, params, true);
        },
        api: {
            locations: {
                /** params: countryId */
                country_regions: '/location/countries/{{countryId}}/regions',
                /** params: regionId */
                region_cities: '/location/regions/{{regionId}}/cities',
                city: '/location/city'
            },
            tasks: {
                form_data: '/private/tasks/form-data',
                /** params: query */
                search: '/tasks/search?q={{query}}'
            }
        },
        html: {
            tasks: {
                /** params: slug */
                view: '/tasks/{{slug}}'
            }
        }
    };

    return app;
}(App || {}, $));

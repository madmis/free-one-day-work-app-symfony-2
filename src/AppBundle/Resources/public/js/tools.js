var App = (function (app, $) {
    "use strict";

    app.tools = {
        /**
         * Render template
         * @param {String} template
         * @param {Object} data
         * @returns {String}
         */
        render: function (template, data) {
            for (var key in data) {
                if (!data.hasOwnProperty(key)) {
                    continue;
                }
                var value = data[key].call ? data[key]() : data[key];
                template = template.replace(new RegExp('\{\{' + key + '\}\}', 'g'), value)
            }

            return template;
        }
    };

    return app;
}(App || {}, $));


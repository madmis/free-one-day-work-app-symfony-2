var App = (function (app, $) {
    "use strict";

    app.profile = {
        el: {
            form: $('#profile-form'),
            country: $('#fos_user_profile_form_country'),
            region: $('#fos_user_profile_form_region'),
            city: $('#fos_user_profile_form_city'),
            timeZone: $('#fos_user_profile_form_timeZone')
        },
        validationOptions: {
            rules: {
                // simple rule, converted to {required:true}
                'fos_user_profile_form[username]': "required",
                'fos_user_profile_form[email]': {
                    required: true,
                    email: true
                }
            },
            errorPlacement: function(error, element) {
                error.addClass('control-label');
                element.closest('.form-group')
                    .addClass('has-error')
                    .append(error)
                    .find('.help-block').remove();
            }
        },
        formHandler: function() {
            var self = this;
            $(function () {
                //self.el.form.validate(self.validationOptions);
                //self.el.form.on('submit', function() {
                //    if (!self.el.form.valid()) {
                //        return false;
                //    }
                //});

                self.initCountrySelect();
                self.initRegionsSelect();
                self.initCitiesSelect();
                self.el.timeZone.select2();
            });
        },
        initCountrySelect: function() {
            var self = this;
            self.el.country.select2();
            self.el.country.on("change", function (e) {
                if ($(e.target).val()) {
                    self.el.region.prop('disabled', false);
                }
            });
        },
        initRegionsSelect: function() {
            var self = this;
            self.el.region.select2({
                ajax: {
                    dataType: app.config.requestsDataType,
                    url:  function (params) {
                        var options = { countryId: self.el.country.val() };
                        return app.routing.buildUrl(app.routing.api.locations.country_regions, options);
                    },
                    processResults: function (response) {
                        return {
                            results: $.map(response.data, function (item) {
                                return { text: item.name, id: item.id }
                            })
                        };
                    }
                },
                minimumInputLength: 2
            });

            self.el.region.prop('disabled', !self.el.country.val());

            self.el.region.on("change", function (e) {
                if ($(e.target).val()) {
                    self.el.city.prop('disabled', false);
                }
            });
        },
        initCitiesSelect: function() {
            var self = this;

            self.el.city.select2({
                ajax: {
                    dataType: app.config.requestsDataType,
                    url:  function (params) {
                        var options = { regionId: self.el.region.val() };
                        return app.routing.buildUrl(app.routing.api.locations.region_cities, options);
                    },
                    processResults: function (response) {
                        return {
                            results: $.map(response.data, function (item) {
                                return { text: item.name, id: item.id }
                            })
                        };
                    }
                },
                minimumInputLength: 2
            });
            self.el.city.prop('disabled', !self.el.region.val());
        }
    };

    app.profile.formHandler();

    return app;
}(App || {}, $));
var App = (function (app, $) {
    "use strict";

    app = {
        config: {
            requestsDataType: 'json'
        },
        initBootstrap: function() {
            $.fn.select2.defaults.set("theme", "bootstrap");
        },
        /**
         * Global loading on ajax
         */
        globalLoader: function() {
            $('#global-loader').hide();
            $(document).ajaxStart(function () {
                $('#global-loader').show();
            }).ajaxSend(function( event, jqxhr, settings ) {
                $('#global-loader').show();
            }).ajaxStop(function () {
                $('#global-loader').hide();
            });
        }
    };

    app.globalLoader();
    app.initBootstrap();

    return app;
}(App || {}, $));

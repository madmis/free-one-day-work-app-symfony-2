<?php

namespace AppBundle\OAuth\UserFactory;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class FacebookUser
 * @package AppBundle\OAuth\UserFactory
 */
class FacebookUser extends AbstractServiceUser
{
    /**
     * @inheritdoc
     */
    protected function setAdditionalData(User $user)
    {
        $response = new ParameterBag($this->response->getResponse());
        $user->setFirstName($response->get('first_name', ''));
        $user->setLastName($response->get('last_name', ''));
    }
}
<?php

namespace AppBundle\OAuth\UserFactory;

use AppBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;

/**
 * Interface ServiceUserInterface
 * @package AppBundle\OAuth\UserFactory
 */
interface ServiceUserInterface
{
    /**
     * @param UserManagerInterface $userManager
     * @param UserResponseInterface $response
     */
    public function __construct(UserManagerInterface $userManager, UserResponseInterface $response);

    /**
     * @return User
     */
    public function createUser();
}
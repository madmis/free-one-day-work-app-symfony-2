<?php

namespace AppBundle\OAuth\UserFactory;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class VkUser
 * @package AppBundle\OAuth\UserFactory
 */
class VkUser extends AbstractServiceUser
{
    /**
     * @inheritdoc
     */
    protected function setAdditionalData(User $user)
    {
        $user->setFirstName('');
        $user->setLastName('');

        $response = $this->response->getResponse();
        if (!empty($response['response'][0]['first_name'])) {
            $user->setFirstName($response['response'][0]['first_name']);
        }

        if (!empty($response['response'][0]['last_name'])) {
            $user->setLastName($response['response'][0]['last_name']);
        }
    }
}
<?php

namespace AppBundle\OAuth\UserFactory;
use AppBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;

/**
 * Class AbstractServiceUser
 * @package AppBundle\OAuth\UserFactory
 */
abstract class AbstractServiceUser implements ServiceUserInterface
{
    /**
     * @var UserResponseInterface
     */
    protected $response;

    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @param UserManagerInterface $userManager
     * @param UserResponseInterface $response
     */
    public function __construct(UserManagerInterface $userManager, UserResponseInterface $response)
    {
        $this->response = $response;
        $this->userManager = $userManager;
    }

    /**
     * @param User $user
     * @return void
     */
    protected abstract function setAdditionalData(User $user);

    /**
     * @return User
     */
    public function createUser()
    {
        $user = new User();
        $email = $this->getEmail();
        $username = $this->response->getNickname();

        $user->setEmail($email);
        if ($this->userManager->findUserByUsername($username)) {
            $user->setUsername($email);
        } else {
            $user->setUsername($username);
        }

        $picture = $this->uploadProfileImage($user->getUploadRootDir());
        $user->setProfileImage($picture);

        $this->setAdditionalData($user);

        return $user;
    }

    /**
     * @return string
     */
    protected function getEmail()
    {
        $id = $this->response->getUsername();
        $serviceName = $this->response->getResourceOwner()->getName();

        $email = $this->response->getEmail();
        if ($email === null) {
            $email = sprintf('%s@%s', $id, $serviceName);
        }

        return $email;
    }

    /**
     * @param string $uploadPath
     * @return null|string profile image name
     */
    protected function uploadProfileImage($uploadPath)
    {
        $pictureUrl = $this->response->getProfilePicture();
        if ($pictureUrl !== null) {
            $extension = pathinfo($pictureUrl, PATHINFO_EXTENSION);
            if (empty($extension)) {
                $extension = 'jpg';
            }
            $fileName = sprintf('%s.%s', md5(uniqid(rand(), true)), $extension);
            $uploadPath = sprintf('%s/%s', $uploadPath, $fileName);

            if (copy($pictureUrl, $uploadPath)) {
                return $fileName;
            }
        }

        return null;
    }
}
<?php

namespace AppBundle\OAuth\UserFactory;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class TwitterUser
 * @package AppBundle\OAuth\UserFactory
 */
class TwitterUser extends AbstractServiceUser
{
    /**
     * @inheritdoc
     */
    protected function setAdditionalData(User $user)
    {
        $response = new ParameterBag($this->response->getResponse());
        $user->setFirstName($response->get('name', ''));
        $user->setLastName('');
    }
}
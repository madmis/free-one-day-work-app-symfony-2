<?php

namespace AppBundle\OAuth\UserFactory;

use AppBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;

/**
 * Class ServiceUserFactory
 * @package AppBundle\OAuth\UserFactory
 */
class ServiceUserFactory
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @param UserManagerInterface $userManager
     */
    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param UserResponseInterface $response
     * @return User
     * @throws \RuntimeException if factory class not exists
     */
    public function createUser(UserResponseInterface $response)
    {
        $serviceName = $response->getResourceOwner()->getName();
        $className = sprintf('AppBundle\OAuth\UserFactory\%sUser', ucfirst($serviceName));

        if (!class_exists($className)) {
            $msg = 'Undefined user class for service "%s". Define user class "%s" for this service.';
            throw new \RuntimeException(sprintf($msg, $serviceName, $className));
        }

        /** @var ServiceUserInterface $class */
        $class = new $className($this->userManager, $response);

        return $class->createUser();
    }
}
<?php

namespace AppBundle\OAuth\UserFactory;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class GoogleUser
 * @package AppBundle\OAuth\UserFactory
 */
class GoogleUser extends AbstractServiceUser
{
    /**
     * @inheritdoc
     */
    protected function setAdditionalData(User $user)
    {
        $response = new ParameterBag($this->response->getResponse());
        $user->setFirstName($response->get('given_name', ''));
        $user->setLastName($response->get('family_name', ''));
    }
}
<?php

namespace AppBundle\OAuth\UserFactory;

use AppBundle\Entity\User;

/**
 * Class LinkedinUser
 * @package AppBundle\OAuth\UserFactory
 */
class LinkedinUser extends AbstractServiceUser
{
    /**
     * @inheritdoc
     */
    protected function setAdditionalData(User $user)
    {
        $name = $this->response->getRealName();
        $user->setFirstName($name ?: '');
        $user->setLastName('');
    }
}
<?php

namespace AppBundle\Event\Listener;

use AppBundle\Entity\Interfaces\TranslatableInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class EntityTranslatableListener
 * @package AppBundle\Event\Listener
 */
class EntityTranslatableListener
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param TranslatableInterface $entity
     * @param LifecycleEventArgs $event
     */
    public function prePersist(TranslatableInterface $entity, LifecycleEventArgs $event)
    {
        $entity->setTranslator($this->translator);
    }

    /**
     * @param TranslatableInterface $entity
     * @param LifecycleEventArgs $event
     */
    public function postLoad(TranslatableInterface $entity, LifecycleEventArgs $event)
    {
        $entity->setTranslator($this->translator);
    }
}
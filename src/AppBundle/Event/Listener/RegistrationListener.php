<?php

namespace AppBundle\Event\Listener;

use AppBundle\Entity\User;
use AppBundle\FlashTypes;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class RegistrationListener
 * @package AppBundle\Event\Listener
 */
class RegistrationListener implements EventSubscriberInterface
{
    /**
     * @var HttpUtils
     */
    private $utils;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param HttpUtils $utils
     * @param FlashBagInterface $flashBag
     * @param TranslatorInterface $translator
     */
    public function __construct(HttpUtils $utils, FlashBagInterface $flashBag, TranslatorInterface $translator)
    {
        $this->utils = $utils;
        $this->flashBag = $flashBag;
        $this->translator = $translator;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_CONFIRM => 'onRegistrationConfirm',
        ];
    }

    public function onRegistrationConfirm(GetResponseUserEvent $event)
    {
        /** @var User $user */
        $user = $event->getUser();
        $user->setEmailConfirmed(true);

        $response = $this->utils->createRedirectResponse($event->getRequest(), 'fos_user_profile_show');
        $event->setResponse($response);

        $msg = $this->translator->trans('message.confirmed.email');
        $this->flashBag->add(FlashTypes::SUCCESS, $msg);
    }
}
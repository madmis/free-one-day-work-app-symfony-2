<?php


namespace AppBundle\Event\Listener;

use AppBundle\Entity\Task;
use AppBundle\Entity\TaskFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Gregwar\ImageBundle\ImageHandler;
use Gregwar\ImageBundle\Services\ImageHandling;
use League\Flysystem\Filesystem;

/**
 * Class EntityTaskListener
 * @package AppBundle\Event\Listener
 */
class EntityTaskListener
{
    /**
     * @var ImageHandling
     */
    private $imageHandling;

    /**
     * @var Filesystem
     */
    private $fileStorage;

    /**
     * @param Filesystem $fileStorage
     * @param ImageHandling $imageHandling
     */
    public function __construct(Filesystem $fileStorage, ImageHandling $imageHandling)
    {
        $this->fileStorage = $fileStorage;
        $this->imageHandling = $imageHandling;
    }

    /**
     * @param Task $task
     * @param LifecycleEventArgs $event
     */
    public function prePersist(Task $task, LifecycleEventArgs $event)
    {
        foreach($task->getFiles() as $taskFile) {
            $pathName = $taskFile->getFile()->getPathname();
            /** @var ImageHandler $img */
            $img = $this->imageHandling->open($pathName);
            $contents = $img->cropResize(TaskFile::MAX_WIDTH, TaskFile::MAX_HEIGHT)->get();
            $fileName = $taskFile->generateFilename();
            $this->fileStorage->write($fileName, $contents);

            $taskFile->setName($fileName);
            $taskFile->setTask($task);
            $event->getEntityManager()->persist($taskFile);
        }
    }

    public function postPersist(Task $task, LifecycleEventArgs $event)
    {
    }

    public function postRemove(Task $task, LifecycleEventArgs $event)
    {
    }

    public function preRemove(Task $task, LifecycleEventArgs $event)
    {
    }

    public function preFlush(Task $task, PreFlushEventArgs $event)
    {
    }
}
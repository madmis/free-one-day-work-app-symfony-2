<?php

namespace AppBundle\Event\Listener;

use AppBundle\Entity\User;
use AppBundle\FlashTypes;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\ValidatorInterface;

/**
 * Class RequestListener
 * @package AppBundle\Event\Listener
 */
class RequestListener
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var HttpUtils
     */
    private $httpUtils;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param FlashBagInterface $flashBag
     * @param TranslatorInterface $translator
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        FlashBagInterface $flashBag,
        TranslatorInterface $translator,
        HttpUtils $httpUtils,
        ValidatorInterface $validator)
    {
        $this->tokenStorage = $tokenStorage;
        $this->flashBag = $flashBag;
        $this->translator = $translator;
        $this->httpUtils = $httpUtils;
        $this->validator = $validator;
    }

    /**
     * @return null|User
     */
    private function getUser()
    {
        $token = $this->tokenStorage->getToken();
        if ($token != null) {
            $user = $token->getUser();

            return $user;
        }

        return null;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $user = $this->getUser();
        if ($user instanceof User) {
            $request = $event->getRequest();
            $errors = $this->validator->validate($user, ['FillProfile', 'Profile']);
            if (!$request->isXmlHttpRequest() && count($errors) > 0 && $request->get('_route') != 'fos_user_profile_edit') {
                // redirect to edit profile
                $response = $this->httpUtils->createRedirectResponse($event->getRequest(), 'fos_user_profile_edit');
                $msg = $this->translator->trans('message.profile.fill.required_fields');
                $this->flashBag->add(FlashTypes::WARNING, $msg);
                $event->setResponse($response);
            }
        }
    }
}
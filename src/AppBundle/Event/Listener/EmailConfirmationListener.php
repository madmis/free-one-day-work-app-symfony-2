<?php

namespace AppBundle\Event\Listener;

use AppBundle\Event\AppEvents;
use AppBundle\Event\UserEvent;
use AppBundle\FlashTypes;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class EmailConfirmationListener
 * @package AppBundle\Event\Listener
 */
class EmailConfirmationListener implements EventSubscriberInterface
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param MailerInterface $mailer
     * @param TokenGeneratorInterface $tokenGenerator
     * @param SessionInterface $session
     * @param FlashBagInterface $flashBag
     * @param TranslatorInterface $translator
     */
    public function __construct(
        MailerInterface $mailer,
        TokenGeneratorInterface $tokenGenerator,
        SessionInterface $session,
        FlashBagInterface $flashBag,
        TranslatorInterface $translator) {

        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
        $this->session = $session;
        $this->flashBag = $flashBag;
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AppEvents::EMAIL_CONFIRMATION => 'onEmailConfirmation',
        ];
    }

    /**
     * @param UserEvent $event
     */
    public function onEmailConfirmation(UserEvent $event)
    {
        $user = $event->getUser();

        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $this->mailer->sendConfirmationEmailMessage($user);

        $msg = $this->translator->trans('message.confirm.email');
        $this->flashBag->add(FlashTypes::INFO, $msg);
        $this->session->set('fos_user_send_confirmation_email/email', $user->getEmail());
    }
}
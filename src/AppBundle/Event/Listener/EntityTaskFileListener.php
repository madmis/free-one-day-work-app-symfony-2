<?php

namespace AppBundle\Event\Listener;

use AppBundle\Entity\TaskFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use League\Flysystem\File;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;

/**
 * Class EntityTaskFileListener
 * @package AppBundle\Event\Listener
 */
class EntityTaskFileListener
{
    /**
     * @var Filesystem
     */
    private $fileStorage;

    /**
     * @var string
     */
    private $defaultImgUrl;

    /**
     * @param Filesystem $fileStorage
     */
    public function __construct(Filesystem $fileStorage, $defaultImgUrl)
    {
        $this->fileStorage = $fileStorage;
        $this->defaultImgUrl = $defaultImgUrl;
    }

    public function postLoad(TaskFile $entity, LifecycleEventArgs $event)
    {
        /** @var File $file */
        try {
            $file = $this->fileStorage->get($entity->getName());
            $entity->setDataUrlImage($file);
        } catch (FileNotFoundException $ex) {
            $entity->setDataUrlImage($this->defaultImgUrl);
        }
    }
}
<?php

namespace AppBundle\Event\Listener;

use AppBundle\Entity\User;
use AppBundle\Event\AppEvents;
use AppBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ProfileChangedListener
 * @package AppBundle\Event\Listener
 */
class ProfileChangedListener implements EventSubscriberInterface
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @param UserManagerInterface $userManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(UserManagerInterface $userManager, EventDispatcherInterface $dispatcher)
    {
        $this->userManager = $userManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::PROFILE_EDIT_SUCCESS => 'onProfileEditSuccess',
            FOSUserEvents::PROFILE_EDIT_COMPLETED => 'onProfileEditCompleted',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onProfileEditSuccess(FormEvent $event)
    {
        /** @var User $data */
        $data = $event->getForm()->getData();

        if ($data instanceof User) {
            /** @var User $user */
            $user = $this->userManager->findUserBy(['id' => $data->getId()]);

            if (!$user->isEmailConfirmed() || $data->getEmail() != $user->getEmail()) {
                $event = new UserEvent($user);
                $this->dispatcher->dispatch(AppEvents::EMAIL_CONFIRMATION, $event);
            }
        }
    }

    public function onProfileEditCompleted(FilterUserResponseEvent $event)
    {
    }
}
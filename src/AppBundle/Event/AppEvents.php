<?php

namespace AppBundle\Event;

/**
 * Class AppEvents
 * @package AppBundle\Event
 */
class AppEvents {

    /**
     * The EMAIL_CONFIRMATION event occurs when the registration successful.
     */
    const EMAIL_CONFIRMATION = 'app.email.confirmation';

}
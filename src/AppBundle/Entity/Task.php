<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * Class Task
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\TaskRepository")
 * @ORM\Table(name="task")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\EntityListeners({"AppBundle\Event\Listener\EntityTaskListener"})
 *
 * @ExclusionPolicy("all")
 */
class Task
{
    use TimestampableEntity;

    const STATUS_ACTIVE = 'active';
    const STATUS_FINISHED = 'finished';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     * @var string
     * @Assert\NotBlank()
     * @Expose
     */
    protected $title;

    /**
     * @ORM\Column(name="description", type="text", nullable=false)
     * @var string
     * @Assert\NotBlank()
     * @Expose
     */
    protected $description;

    /**
     * @ORM\Column(name="currency", type="string", length=3, nullable=false)
     * @var string
     * @Assert\NotBlank()
     * @Expose
     */
    protected $currency;

    /**
     * @ORM\Column(name="budget", type="integer", length=11, nullable=false)
     * @var int
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value = 0)
     * @Expose
     */
    protected $budget;

    /**
     * @ORM\Column(name="status", type="string", nullable=false, options={ "default" = "active" })
     * @var string
     * @Expose
     */
    protected $status = self::STATUS_ACTIVE;

    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * @var Country
     * @Expose
     **/
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * @var Region
     * @Expose
     **/
    protected $region;

    /**
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * @var City
     * @Expose
     **/
    protected $city;

    /**
     * @ORM\ManyToMany(targetEntity="Category")
     * @var ArrayCollection|Category[]
     * @Assert\Count(min = "1", max = "3", minMessage="task.categories.choose.least.one")
     */
    protected $categories;

    /**
     * @ORM\OneToMany(targetEntity="TaskFile", mappedBy="task")
     * @var ArrayCollection|TaskFile[]
     * @Assert\Valid()
     */
    protected $files;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tasks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @var User
     **/
    protected $user;

    /**
     * @Gedmo\Slug(fields={"id", "title"})
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     * @Expose
     */
    private $slug;

    public function __construct()
    {
        $this->status = self::STATUS_ACTIVE;
        $this->categories = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param int $budget
     * @return $this
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param Region $region
     * @return $this
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return TaskFile[]|ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param ArrayCollection|TaskFile[] $files
     * @return $this
     */
    public function setFiles(ArrayCollection $files)
    {
        $this->files = $files;
        return $this;
    }

    /**
     * @param TaskFile $file
     * @return $this
     */
    public function removeFile(TaskFile $file)
    {
        $this->files->removeElement($file);
        return $this;
    }

    /**
     * @param TaskFile $file
     * @return $this
     */
    public function addFile(TaskFile $file)
    {
        if (!$this->files->contains($file)) {
            $this->files->add($file);
        }

        return $this;
    }

    /**
     * @return Category[]|ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param ArrayCollection $categories
     * @return $this
     */
    public function setCategories(ArrayCollection $categories)
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
        return $this;
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function addCategory(Category $category)
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
<?php

namespace AppBundle\Entity\Interfaces;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class TranslatableInterface
 * @package AppBundle\Entity\Interfaces
 */
interface TranslatableInterface
{
    /**
     * @param TranslatorInterface $translator
     * @return mixed
     */
    public function setTranslator(TranslatorInterface $translator);

    /**
     * Return translated text
     * @return string
     */
    public function __toString();
}
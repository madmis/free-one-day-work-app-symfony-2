<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(groups={"FillProfile"})
     * @var string
     */
    protected $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(groups={"FillProfile"})
     * @var string
     */
    protected $lastName;

    /**
     * @ORM\Column(name="profile_image", type="string", length=255, nullable=true)
     * @var string
     */
    protected $profileImage;

    /**
     * @ORM\Column(name="email_confirmed", type="boolean", nullable=false, options={ "default" = false })
     * @var bool
     */
    protected $emailConfirmed;

    /**
     * @ORM\OneToMany(targetEntity="UserIdentity", mappedBy="user")
     * @var UserIdentity[]|ArrayCollection
     */
    private $identities;

    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"FillProfile"})
     * @var Country
     **/
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"FillProfile"})
     * @var Region
     **/
    protected $region;

    /**
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"FillProfile"})
     * @var City
     **/
    protected $city;

    /**
     * @ORM\Column(name="time_zone", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"FillProfile"})
     * @var string
     */
    protected $timeZone;

    /**
     * @ORM\OneToMany(targetEntity="Task", mappedBy="user")
     * @var Task[]|ArrayCollection
     */
    private $tasks;

    public function __construct()
    {
        parent::__construct();
        $this->identities = new ArrayCollection();
        $this->tasks = new ArrayCollection();
        $this->emailConfirmed = false;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return sprintf('%s %s', $this->getFirstName(), $this->getLastName());
    }

    /**
     * @return string
     */
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * @param string $profileImage
     * @return $this
     */
    public function setProfileImage($profileImage)
    {
        $this->profileImage = $profileImage;
        return $this;
    }

    /**
     * @return UserIdentity[]|ArrayCollection
     */
    public function getIdentities()
    {
        return $this->identities;
    }

    /**
     * @param ArrayCollection $identities
     * @return $this
     */
    public function setIdentities($identities)
    {
        $this->identities = $identities;
        return $this;
    }

    /**
     * @param UserIdentity $identity
     * @return $this
     */
    public function removeIdentity(UserIdentity $identity)
    {
        $this->identities->removeElement($identity);
        return $this;
    }

    /**
     * @param UserIdentity $identity
     * @return $this
     */
    public function addIdentity(UserIdentity $identity)
    {
        if (!$this->identities->contains($identity)) {
            $this->identities->add($identity);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailConfirmed()
    {
        return $this->emailConfirmed;
    }

    /**
     * @param bool $emailConfirmed
     * @return $this
     */
    public function setEmailConfirmed($emailConfirmed)
    {
        $this->emailConfirmed = $emailConfirmed;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRequiredEmailConfirmation()
    {
         return (!$this->isEmailConfirmed() && $this->getConfirmationToken() != null);
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     * @return $this
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     * @return $this
     */
    public function setCity(City $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param Region $region
     * @return $this
     */
    public function setRegion(Region $region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param string $timeZone
     * @return $this
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->profileImage
            ? null
            : $this->getUploadRootDir().'/'.$this->profileImage;
    }

    /**
     * @return null|string
     */
    public function getWebPath()
    {
        return null === $this->profileImage
            ? null
            : $this->getUploadDir().'/'.$this->profileImage;
    }

    /**
     * @return string
     */
    public function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/profile/image';
    }

    /**
     * @return Task[]|ArrayCollection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param ArrayCollection $tasks
     * @return $this
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
        return $this;
    }

    /**
     * @param Task $task
     * @return $this
     */
    public function removeTask(Task $task)
    {
        $this->tasks->removeElement($task);
        return $this;
    }

    /**
     * @param Task $task
     * @return $this
     */
    public function addTask(Task $task)
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks->add($task);
        }

        return $this;
    }
}
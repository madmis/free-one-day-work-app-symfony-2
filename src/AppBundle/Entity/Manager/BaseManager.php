<?php

namespace AppBundle\Entity\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * Class BaseManager
 * @package AppBundle\Entity\Manager
 */
abstract class BaseManager
{
    /**
     * @var EntityManager
     */
    private $_em;

    /**
     * @var EntityRepository
     */
    private $_repository;

    /**
     * @param EntityManager $em
     * @param EntityRepository $repository
     */
    public function __construct(EntityManager $em, EntityRepository $repository)
    {
        $this->_em = $em;
        $this->_repository = $repository;
    }

    /**
     * @return EntityManager
     */
    public function em()
    {
        return $this->_em;
    }

    /**
     * @return EntityRepository
     */
    public function repository()
    {
        return $this->_repository;
    }
}

<?php

namespace AppBundle\Entity\Manager;

use AppBundle\Entity\UserIdentity;

/**
 * Class UserIdentityManager
 * @package AppBundle\Entity\Manager
 */
class UserIdentityManager extends BaseManager
{
    /**
     * @param string $serviceName
     * @param string $serviceUserId
     * @return \AppBundle\Entity\User|null
     */
    public function getUserByService($serviceName, $serviceUserId)
    {
        $criteria = ['serviceName' => $serviceName, 'identity' => $serviceUserId];
        /** @var UserIdentity $identity */
        $identity = $this->repository()->findOneBy($criteria);
        if ($identity !== null) {
            return $identity->getUser();
        }

        return null;
    }
}

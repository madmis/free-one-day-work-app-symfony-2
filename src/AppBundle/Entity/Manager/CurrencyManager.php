<?php

namespace AppBundle\Entity\Manager;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Intl\Intl;

/**
 * Class CurrencyManager
 * @package AppBundle\Entity\Manager
 */
class CurrencyManager
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getCodesList()
    {
        return $this->container->getParameter('currencies');
    }

    /**
     * Get currencies with name and code
     * @return array
     */
    public function getNamedList()
    {
        $currencies = $this->container->getParameter('currencies');
        $result = $names = [];
        foreach($currencies as $code) {
            $names[] = Intl::getCurrencyBundle()->getCurrencyName($code);
            $result[] = [
                'code' => $code,
                'name' => Intl::getCurrencyBundle()->getCurrencyName($code),
            ];
        }

        array_multisort($names, SORT_ASC, $result);

        return $result;
    }
}
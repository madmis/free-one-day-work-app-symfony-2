<?php

namespace AppBundle\Entity\Manager;

use AppBundle\Entity\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class CategoryManager
 * @package AppBundle\Entity\Manager
 */
class CategoryManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param EntityManager $em
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityManager $em, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
        $this->_repository = $em->getRepository('AppBundle\Entity\Category');
        $this->_repository->setTranslator($translator);
    }

    /**
     * @return EntityManager
     */
    public function em()
    {
        return $this->em;
    }

    /**
     * @return CategoryRepository
     */
    public function repository()
    {
        return $this->_repository;
    }
}
<?php

namespace AppBundle\Entity\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class CategoryRepository
 * @package AppBundle\Entity\Repository
 */
class CategoryRepository extends NestedTreeRepository
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @inheritdoc
     */
    public function childrenHierarchy($node = null, $direct = false, array $options = [], $includeNode = false)
    {
        $items = parent::childrenHierarchy($node, $direct, $options, $includeNode);
        $items = $this->setHierarchyTranslate($items);

        return $items;
    }

    /**
     * @param array $items
     * @return array
     */
    private function setHierarchyTranslate(array $items)
    {
        foreach($items as &$item) {
            $item['titleLabel'] = $this->translator->trans($item['title'], [], 'entity-category');

            if (!empty($item['__children']) && is_array($item['__children'])) {
                $item['__children'] = $this->setHierarchyTranslate($item['__children']);
            }
        }

        return $items;
    }
}
<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class TaskRepository
 * @package AppBundle\Entity\Repository
 */
class TaskRepository extends EntityRepository
{
    /**
     * @param string $slug
     * @return \Doctrine\ORM\Query
     */
    public function queryByCategory($slug)
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.categories', 'c', 'WITH', 'c.title = :title')
            ->setParameter(':title', $slug)
            ->getQuery();
    }

    /**
     * @param string $username
     * @return \Doctrine\ORM\Query
     */
    public function queryByOwner($username)
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.user', 'c', 'WITH', 'c.username = :username')
            ->setParameter(':username', $username)
            ->getQuery();
    }

    /**
     * @param string $slug
     * @return \Doctrine\ORM\Query
     */
    public function queryByCountry($slug)
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.country', 'c', 'WITH', 'c.slug = :slug')
            ->setParameter(':slug', $slug)
            ->getQuery();
    }

    /**
     * @param string $slug
     * @return \Doctrine\ORM\Query
     */
    public function queryByCity($slug)
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.city', 'c', 'WITH', 'c.slug = :slug')
            ->setParameter(':slug', $slug)
            ->getQuery();
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function queryAll()
    {
        return $this->createQueryBuilder('t')->getQuery();
    }
}
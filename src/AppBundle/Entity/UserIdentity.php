<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Table(
 *      name="user_identity",
 *      uniqueConstraints={ @ORM\UniqueConstraint(name="unique_idx", columns={"identity", "service_name"}) }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserIdentityRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UserIdentity
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="identities")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @var User
     **/
    protected $user;

    /**
     * @ORM\Column(name="service_name", type="text", nullable=false)
     * @var string
     */
    protected $serviceName;

    /**
     * Service identity
     * @ORM\Column(name="identity", type="text", nullable=false)
     * @var string
     */
    protected $identity;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * @param string $serviceName
     * @return $this
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * @param string $identity
     * @return $this
     */
    public function setIdentity($identity)
    {
        $this->identity = $identity;
        return $this;
    }
}

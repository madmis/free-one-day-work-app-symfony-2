<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use League\Flysystem\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TaskFile
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="task_file")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\EntityListeners({"AppBundle\Event\Listener\EntityTaskFileListener"})
 */
class TaskFile
{
    use TimestampableEntity;

    const MAX_WIDTH = 800;
    const MAX_HEIGHT = 800;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @var UploadedFile
     * @Assert\Image(maxSize = "1024k")
     */
    protected $file;

    /**
     * @ORM\Column(type="string", length=255, name="name")
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="files")
     * @var Task
     */
    protected $task;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function setFile($file = null)
    {
        $this->file = $file;

        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param Task $task
     * @return $this
     */
    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return string
     */
    public function generateFilename()
    {
        $ext = null;
        if ($this->getFile()) {
            $ext = $this->getFile()->getClientOriginalExtension();
        }
        $ext = $ext ?: 'jpg';
        $name = md5(uniqid(rand(), true));

        return sprintf('%s.%s', $name, $ext);
    }

    /**
     * @var string
     */
    private $dataUrlImage;

    /**
     * @param File|string $image File instance or url to image
     */
    public function setDataUrlImage($image)
    {
        if ($image instanceof File) {
            $binary = base64_encode($image->read());
            $this->dataUrlImage = sprintf('data:%s;base64,%s', $image->getMimetype(), $binary);
        } else {
            $this->dataUrlImage = $image;
        }
    }

    /**
     * @return string
     */
    public function getDataUrlImage()
    {
        return $this->dataUrlImage;
    }

}
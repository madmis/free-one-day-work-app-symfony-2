<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TestCommand
 * @package AppBundle\Command
 */
class TestCommand  extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:test');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $r = array_rand(array_flip(range(1, 10)), 5);
        $egg = [3, 5, 8];
        $diff = array_diff(range(1, 10), $r, $egg);

        array_walk($r, function(&$item, $key) use ($egg, &$diff) {
            if (!in_array($item, $egg) && $diff) {
                $key = array_rand($diff);
                $item = $diff[$key];
                unset($diff[$key]);
            }
        });
    }
}
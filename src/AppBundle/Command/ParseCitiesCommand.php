<?php

namespace AppBundle\Command;

use AppBundle\Entity\City;
use AppBundle\Entity\Country;
use AppBundle\Entity\Region;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ParseCitiesCommand
 * @package AppBundle\Command
 */
class ParseCitiesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:parse-cities');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = $this->getParsed();
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $countries = $this->getContainer()->get('manager.country')
            ->repository()->findAll();

        if ($countries) {
            $output->writeln("<info>Table 'country' not empty</info>");
            return;
        }

        foreach($result as $country => $items) {
            $output->writeln("<info>$country</info>");

            ksort($items['regions']);
            $cObj = $items['country'];
            $em->persist($cObj);
            $em->flush();
            $em->clear();

            foreach($items['regions'] as $ritems) {
                $cObj = $em->getRepository('AppBundle:Country')->find($cObj->getId());
                $output->writeln("<info>\t{$ritems['region']->getName()}</info>");
                $rObj = $ritems['region'];
                $rObj->setCountry($cObj);
                $em->persist($rObj);

                ksort($ritems['cities']);
                foreach($ritems['cities'] as $city) {
                    $city->setCountry($cObj);
                    $city->setRegion($rObj);
                    $em->persist($city);
                }
                $em->flush();
                $em->clear();
            }

            $em->flush();
            $em->clear();
        }
    }

    /**
     * @return array
     */
    private function getParsed()
    {
        $dir = $this->getContainer()->getParameter('kernel.root_dir');
        $handle = fopen($dir . '/data/cities.csv', 'r');

        $columns = fgetcsv($handle);

        $result = [];
        while (($data = fgetcsv($handle)) !== false) {
            if (!empty($data[5]) && !empty($data[7]) && !empty($data[10])) {
                $country = $data[5];
                $region = $data[7];

                $cObj = new Country();
                $cObj->setIsoCode(trim($data[4]));
                $cObj->setName(trim($data[5]));

                if (!isset($result[$country])) {
                    $result[$country] = [
                        'country' => $cObj,
                        'regions' => [],
                    ];
                }

                $rObj = new Region();
                $rObj->setName(trim($data[7]));
                $rObj->setCountry($cObj);

                if (!isset($result[$country]['regions'][$region])) {
                    $result[$country]['regions'][$region] = [
                        'region' => $rObj,
                        'cities' => [],
                    ];
                }

                $ctObj = new City();
                $ctObj->setCountry($cObj);
                $ctObj->setRegion($rObj);
                $ctObj->setName(trim($data[10]));
                $ctObj->setTimeZone(trim($data[12]));
                $result[$country]['regions'][$region]['cities'][trim($data[10])] = $ctObj;
            }
        }
        fclose($handle);

        ksort($result);
        return $result;
    }
}
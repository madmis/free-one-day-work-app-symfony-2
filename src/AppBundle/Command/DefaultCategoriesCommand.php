<?php

namespace AppBundle\Command;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DefaultCategoriesCommand
 * @package AppBundle\Command
 */
class DefaultCategoriesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:add-default-categories');
    }

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $items = $this->getContainer()->get('manager.category')->_repository->findAll();
        if ($items) {
            $this->output->writeln('<error>Categories table not empty</error>');
            return;
        }

        $this->output = $output;
        $this->house();
        $this->garden();
        $this->auto();
        $this->parties();
        $this->building();
        $this->education();
        $this->beauty();
    }

    /**
     * @param array $categories
     */
    private function addCategories(array $categories)
    {
        $manager = $this->getContainer()->get('manager.category')->em();
        $translator = $this->getContainer()->get('translator');

        $this->output->writeln("<info>Add: {$categories['parent']}</info>");

        $parent = new Category($translator);
        $parent->setTitle($categories['parent']);
        $manager->persist($parent);

        foreach ($categories['child'] as $childName) {
            $child = new Category($translator);
            $child->setTitle($childName);
            $child->setParent($parent);
            $manager->persist($child);
        }

        $manager->flush();

//        $this->output->writeln($categories['parent']);
//        foreach ($categories['child'] as $childName) {
//            $this->output->writeln($childName);
//        }
    }

    private function house()
    {
        $categories = [
            'parent' => 'house',
            'child' => [
                'house.cleaning',
                'house.fun',
                'house.nanny',
                'house.cakes',
                'house.interior',
                'house.air_conditioning', // кондиционеры
                'house.other',
            ]
        ];
        $this->addCategories($categories);
    }

    private function garden()
    {
        $categories = [
            'parent' => 'garden',
            'child' => [
                'garden.cleaning',
                'garden.ingathering',
                'garden.other',
            ]
        ];
        $this->addCategories($categories);
    }

    private function auto()
    {
        $categories = [
            'parent' => 'auto',
            'child' => [
                'auto.freight',
                'auto.repair',
                'auto.other',
            ]
        ];
        $this->addCategories($categories);
    }

    private function parties()
    {
        $categories = [
            'parent' => 'parties', //Parties and Events
            'child' => [
                'parties.preparation', //preparation parties | подготовка и проведение праздников
                'parties.music',
                'parties.photographs',
                'parties.other',
            ]
        ];
        $this->addCategories($categories);
    }

    private function building()
    {
        $categories = [
            'parent' => 'building', //Строительство
            'child' => [
                'building.repair', //preparation parties | подготовка и проведение праздников
                'building.electrician', // электрика
                'building.carpentry', // столярка
                'building.balconies', // Balconies and Loggias | Балконы и лоджии
                'building.other',
            ]
        ];
        $this->addCategories($categories);
    }

    private function education()
    {
        $categories = [
            'parent' => 'education',
            'child' => [
                'education.foreign', //foreign languages
                'education.tutor', // репетитор
                'education.it_courses',
                'education.other',
            ]
        ];
        $this->addCategories($categories);
    }

    private function beauty()
    {
        $categories = [
            'parent' => 'beauty',
            'child' => [
                'beauty.manicure',
                'beauty.visagist',
                'beauty.beautician', //косметолог
                'beauty.other',
            ]
        ];
        $this->addCategories($categories);
    }

    private function it()
    {
        $categories = [
            'parent' => 'it',
            'child' => [
                'it.seo',
                'it.wordpress',
                'it.mobile_apps',
                'it.web_development',
                'it.other',
            ]
        ];
        $this->addCategories($categories);
    }
}
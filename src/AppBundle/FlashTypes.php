<?php

namespace AppBundle;

/**
 * Class FlashTypes
 * @package AppBundle
 */
class FlashTypes
{
    /**
     * blue
     */
    const INFO = 'info';
    /**
     * green
     */
    const SUCCESS = 'success';
    /**
     * yellow
     */
    const WARNING = 'warning';
    /**
     * red
     */
    const ERROR = 'danger';
    /**
     * violet
     */
    const ROYAL = 'royal';
    /**
     * dark blue
     */
    const PRIMARY = 'primary';
}
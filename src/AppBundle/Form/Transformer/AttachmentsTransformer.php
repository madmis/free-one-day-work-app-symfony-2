<?php

namespace AppBundle\Form\Transformer;

use AppBundle\Entity\TaskFile;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AttachmentsTransformer
 * @package AppBundle\Form\Transformer
 */
class AttachmentsTransformer implements DataTransformerInterface
{
    public function transform($value)
    {
        $v = $value;
        // TODO: Implement transform() method.
    }

    /**
     * @param UploadedFile[] $files
     * @return array
     */
    public function reverseTransform($files)
    {
        $images = [];
        foreach($files as $file){
            $attachment = new TaskFile();
            $attachment->setFile($file);
            $attachment->setName($file->getClientOriginalName());
            $images[] = $attachment;
        }
        return $images;
    }
}
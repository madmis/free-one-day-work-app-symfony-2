<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Manager\CategoryManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class TaskFormType
 * @package AppBundle\Form\Type
 */
class TaskFormType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text', [
            'required' => true,
            'label' => 'tasks.form.title',
            'translation_domain' => 'tasks',
        ]);
        $builder->add('description', 'textarea', [
            'required' => true,
            'label' => 'tasks.form.description',
            'translation_domain' => 'tasks',
        ]);
        $builder->add('currency', 'currency', [
            'required' => true,
            'label' => 'tasks.form.currency',
            'translation_domain' => 'tasks',
        ]);
        $builder->add('budget', 'integer', [
            'required' => true,
            'label' => 'tasks.form.budget',
            'translation_domain' => 'tasks',
        ]);
        $builder->add(
            $builder->create("files", 'collection', [
                'type' => new TaskFileType(),
                'allow_add' => true,
                'label' => 'tasks.form.files',
                'translation_domain' => 'tasks',
            ])
        );
        $builder->add('country', 'entity', [
            'class' => 'AppBundle:Country',
            'label' => 'tasks.form.country',
            'translation_domain' => 'tasks',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c')->setMaxResults(1); // don't load all data
            },
        ]);
        $builder->add('region', 'entity', [
            'class' => 'AppBundle:Region',
            'label' => 'tasks.form.region',
            'translation_domain' => 'tasks',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('r')->setMaxResults(1); // don't load all data
            },
        ]);
        $builder->add('city', 'entity', [
            'class' => 'AppBundle:City',
            'label' => 'tasks.form.city',
            'translation_domain' => 'tasks',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c')->setMaxResults(1); // don't load all data
            },
        ]);
        $builder->add('categories', 'entity', [
            'class' => 'AppBundle:Category',
            'expanded' => true,
            'multiple' => true,
            'required' => true,
            'label' => 'tasks.form.categories',
            'translation_domain' => 'tasks',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_task_form';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
        ]);
    }
}
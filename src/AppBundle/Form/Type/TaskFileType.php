<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\TaskFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class TaskFileType
 * @package AppBundle\Form\Type
 */
class TaskFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            $builder->create('file', 'file', [])
        );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\TaskFile',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'task_file_type';
    }
}
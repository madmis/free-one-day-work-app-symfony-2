<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Manager\CityManager;
use AppBundle\Entity\Manager\RegionManager;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ProfileFormType
 * @package AppBundle\Form\Type
 */
class ProfileFormType extends AbstractType
{
    /**
     * @var RegionManager
     */
    private $rm;

    /**
     * @var CityManager
     */
    private $cm;

    public function __construct(RegionManager $rm, CityManager $cm)
    {
        $this->rm = $rm;
        $this->cm = $cm;
    }

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', 'text');
        $builder->add('lastName', 'text');
        $builder->add('country', 'entity', [
            'placeholder' => $this->translator->trans('form.profile.select.country'),
            'class' => 'AppBundle\Entity\Country',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c')
                    ->orderBy('c.name', 'ASC');
            },
        ]);
        $builder->add('timeZone', 'choice', [
            'empty_value' => $this->translator->trans('form.profile.select.time_zone'),
            'choices' => array_combine(\DateTimeZone::listIdentifiers(), \DateTimeZone::listIdentifiers()),
        ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            // this would be your entity, i.e. SportMeetup
            $data = $event->getData();
            $this->addRegion($event);
            $this->addCity($event);

            if ($data instanceof User) {
                if ($data->getCountry() !== null) {
                    $this->addRegion($event, $data->getCountry()->getId());
                }

                if ($data->getRegion() !== null) {
                    $this->addCity($event, $data->getRegion()->getId());
                }
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            if (!empty($data['country'])) {
                $this->addRegion($event, (int)$data['country']);
            }

            if (!empty($data['region'])) {
                $this->addCity($event, (int)$data['region']);
            }
        });
    }

    /**
     * @param FormEvent $event
     * @param int $countryId
     */
    private function addRegion(FormEvent $event, $countryId = null)
    {
        $regions = [];
        if ($countryId) {
            $conditions = ['country' => $countryId];
            $regions = $this->rm->repository()->findBy($conditions, ['name' => 'ASC']);
        }

        $event->getForm()->add('region', 'entity', [
            'class' => 'AppBundle\Entity\Region',
            'placeholder' => $this->translator->trans('form.profile.select.region'),
            'choices' => $regions,
        ]);
    }

    /**
     * @param FormEvent $event
     * @param int $regionId
     */
    private function addCity(FormEvent $event, $regionId = null)
    {
        $cities = [];
        if ($regionId) {
            $conditions = ['region' => $regionId];
            $cities = $this->cm->repository()->findBy($conditions, ['name' => 'ASC']);
        }

        $event->getForm()->add('city', 'entity', [
            'class' => 'AppBundle\Entity\City',
            'placeholder' => $this->translator->trans('form.profile.select.city'),
            'choices' => $cities,
        ]);
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return 'fos_user_profile';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_user_profile_form';
    }
}
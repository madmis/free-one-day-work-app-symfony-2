<?php

namespace AppBundle\Security\User\Provider;

use AppBundle\Entity\Manager\UserIdentityManager;
use AppBundle\Entity\User;
use AppBundle\Entity\UserIdentity;
use AppBundle\Event\AppEvents;
use AppBundle\Event\UserEvent;
use AppBundle\OAuth\UserFactory\ServiceUserFactory;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class FosUserProvider
 * @package AppBundle\Security\User\Provider
 */
class FosUserProvider extends FOSUBUserProvider
{
    /**
     * @var UserIdentityManager
     */
    private $userIdentityManager;

    /**
     * @var ServiceUserFactory
     */
    private $factory;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var bool
     */
    private $confirmationEnabled = false;

    /**
     * Register user here, if user not exists
     * @param UserResponseInterface $response
     * @return \FOS\UserBundle\Model\UserInterface
     * @throws AccountNotLinkedException
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        if ($username === null) {
            throw new AccountNotLinkedException(sprintf("User '%s' not found.", $username));
        }

        $serviceName = $response->getResourceOwner()->getName();
        $serviceUserId = $response->getUsername();
        $user = $this->userIdentityManager->getUserByService($serviceName, $serviceUserId);

        if ($user === null) {
            $user = $this->createUser($response);
        }

        return $user;
    }

    /**
     * @param UserResponseInterface $response
     * @return User
     */
    private function createUser(UserResponseInterface $response)
    {
        $user = $this->factory->createUser($response);
        if ($this->userManager->findUserByEmail($user->getEmail())) {
            $msg = 'User with same email "%s" exists.';
            throw new AccountNotLinkedException(sprintf($msg, $user->getEmail()));
        }

        $user->setEnabled(true);
        $user->setPassword('');

        $this->userManager->updateUser($user);

        $identity = new UserIdentity();
        $identity->setServiceName($response->getResourceOwner()->getName())
            ->setIdentity($response->getUsername())->setUser($user);
        $this->userIdentityManager->em()->persist($identity);
        $this->userIdentityManager->em()->flush();

        return $user;
    }

    /**
     * @param UserIdentityManager $userIdentityManager
     */
    public function setUserIdentityManager(UserIdentityManager $userIdentityManager)
    {
        $this->userIdentityManager = $userIdentityManager;
    }

    /**
     * @param ServiceUserFactory $factory
     */
    public function setServiceUserFactory(ServiceUserFactory $factory)
    {
        $this->factory = $factory;
    }

    public function setEventDispatcher(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param $enabled
     */
    public function setConfirmationEnabled($enabled)
    {
        $this->confirmationEnabled = $enabled;
    }

    /**
     * @return boolean
     */
    public function isConfirmationEnabled()
    {
        return $this->confirmationEnabled;
    }
}

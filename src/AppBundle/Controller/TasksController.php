<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use AppBundle\Form\Type\TaskFormType;
use Pagerfanta\Exception\OutOfRangeCurrentPageException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class TasksController
 * @package AppBundle\Controller
 */
class TasksController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createTaskAction()
    {
        $form = $form = $this->createForm(new TaskFormType(), new Task());

        return $this->render('AppBundle:Tasks:create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function allTasksAction($page)
    {
        $query = $this->get('manager.task')->repository()->queryAll();

        try {
            $perPage = $this->container->getParameter('tasks_per_page');
            $pager = $this->get('utils.pager')->create($query, $page, $perPage);
        } catch(OutOfRangeCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        return $this->render('AppBundle:Tasks:list.html.twig', [
            'pager' => $pager,
        ]);
    }

    public function viewTaskAction($slug)
    {
        /** @var Task $task */
        $task = $this->get('manager.task')->repository()
            ->findOneBy(['slug' => $slug]);

        if (!$task) {
            throw new NotFoundHttpException('Page not found');
        }

        return $this->render('AppBundle:Tasks:view.html.twig', [
            'task' => $task,
        ]);

    }

    /**
     * @param string $slug
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tasksByCategoryAction($slug, $page)
    {
        $query = $this->get('manager.task')->repository()->queryByCategory($slug);

        try {
            $perPage = $this->container->getParameter('tasks_per_page');
            $pager = $this->get('utils.pager')->create($query, $page, $perPage);
        } catch(OutOfRangeCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        return $this->render('AppBundle:Tasks:list.html.twig', [
            'pager' => $pager,
        ]);
    }

    /**
     * @param string $username
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tasksByOwnerAction($username, $page)
    {
        $query = $this->get('manager.task')->repository()->queryByOwner($username);

        try {
            $perPage = $this->container->getParameter('tasks_per_page');
            $pager = $this->get('utils.pager')->create($query, $page, $perPage);
        } catch(OutOfRangeCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        return $this->render('AppBundle:Tasks:list.html.twig', [
            'pager' => $pager,
        ]);
    }

    /**
     * @param string $slug
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tasksByCountryAction($slug, $page)
    {
        $query = $this->get('manager.task')->repository()->queryByCountry($slug);

        try {
            $perPage = $this->container->getParameter('tasks_per_page');
            $pager = $this->get('utils.pager')->create($query, $page, $perPage);
        } catch(OutOfRangeCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        return $this->render('AppBundle:Tasks:list.html.twig', [
            'pager' => $pager,
        ]);
    }

    /**
     * @param string $slug
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tasksByCityAction($slug, $page)
    {
        $query = $this->get('manager.task')->repository()->queryByCity($slug);

        try {
            $perPage = $this->container->getParameter('tasks_per_page');
            $pager = $this->get('utils.pager')->create($query, $page, $perPage);
        } catch(OutOfRangeCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        return $this->render('AppBundle:Tasks:list.html.twig', [
            'pager' => $pager,
        ]);
    }
}
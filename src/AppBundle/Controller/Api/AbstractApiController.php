<?php

namespace AppBundle\Controller\Api;

use AppBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractXhrController
 * @package Mds\AppBundle\Controller\Xhr
 */
abstract class AbstractApiController extends FOSRestController
{
    /**
     * @param array $messages messages array
     * @param int $httpCode http status code
     * @param array $params messages params
     * @param array $headers messages params
     * @return Response
     */
    public function errorResponse(array $messages, $httpCode, array $params = [], array $headers = [])
    {
        $formatter = $this->get('app.formatter.json_response');
        $headers = array_merge($formatter->securityHeaders(), $headers);
        $data = $formatter->error($messages, $httpCode, $params);

        return $this->handleView($this->view($data, $httpCode, $headers));
    }

    /**
     * @param array $data
     * @param int $httpCode
     * @param array $messages
     * @param array $paging
     * @param array $headers
     * @return Response
     */
    public function successResponse(array $data, $httpCode = Response::HTTP_OK, array $messages = [], array $paging = [], array $headers = [])
    {
        $formatter = $this->get('app.formatter.json_response');
        $headers = array_merge($formatter->securityHeaders(), $headers);
        $data = $formatter->success($data, $messages, $paging);

        return $this->handleView($this->view($data, $httpCode, $headers));
    }
}
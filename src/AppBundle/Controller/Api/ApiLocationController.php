<?php

namespace AppBundle\Controller\Api;
use AppBundle\Entity\City;
use Doctrine\ORM\AbstractQuery;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiLocationController
 * @package AppBundle\Controller\Api
 */
class ApiLocationController extends AbstractApiController
{
    /**
     * @param Request $request
     * @param int $countryId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRegionsAction(Request $request, $countryId)
    {
        $query = $request->query->get('q');

        $qb = $this->get('manager.region')->repository()
            ->createQueryBuilder('c')
            ->where('c.country = :country')
            ->andWhere('lower(c.name) like lower(:query)')
            ->setParameter('country', $countryId)
            ->setParameter('query', "%{$query}%")
            ->orderBy('c.name', 'ASC');

        $regions = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);

        return $this->successResponse($regions);
    }

    /**
     * @param Request $request
     * @param int $regionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRegionCitiesAction(Request $request, $regionId)
    {
        $qb = $this->get('manager.city')->repository()
            ->createQueryBuilder('c')
            ->andWhere('c.region = :region')
            ->setParameter(':region', $regionId)
            ->orderBy('c.name', 'ASC')
            ->setMaxResults(25);

        $query = $request->query->get('q');
        if ($query) {
            $qb->andWhere('lower(c.name) like lower(:query)')
                ->setParameter('query', "%{$query}%");
        }

        return $this->successResponse($qb->getQuery()->execute());
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCityAction(Request $request)
    {
        $query = $request->query->get('q');

        $qb = $this->get('manager.city')->repository()
            ->createQueryBuilder('c')
            ->andWhere('lower(c.name) like lower(:query)')
            ->setParameter('query', "%{$query}%")
            ->orderBy('c.name', 'ASC')
            ->orderBy('c.country', 'ASC')
            ->setMaxResults(50);

        /** @var City[] $cities */
        $cities = $qb->getQuery()->getResult();
        $result = [];

        foreach ($cities as $city) {
            $result[] = [
                'id' => $city->getId(),
                'name' => $city->getName(),
                'country_id' => $city->getCountry()->getId(),
                'country_name' => $city->getCountry()->getName(),
                'region_id' => $city->getRegion()->getId(),
                'region_name' => $city->getRegion()->getName(),
            ];
        }

        return $this->successResponse($result);
    }
}
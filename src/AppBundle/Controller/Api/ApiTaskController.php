<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Task;
use AppBundle\Form\Type\TaskFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiTaskController
 * @package AppBundle\Controller\Api
 */
class ApiTaskController extends AbstractApiController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCreateFormDataAction()
    {
        $categories = $this->get('manager.category')->repository()->childrenHierarchy();
        $currencies = $this->get('manager.currency')->getNamedList();

        return $this->successResponse([
            'categories' => $categories,
            'currencies' => $currencies,
            'maxSelectedCategories' => $this->container->getParameter('max_selected_categories'),
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createTaskAction(Request $request)
    {
        $form = $form = $this->createForm(new TaskFormType, new Task);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            $errors = $this->get('utils.form')->getErrorsMessages($form);
            return $this->errorResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        /** @var Task $data */
        $data = $form->getData();
        $data->setUser($this->getUser());
        $this->get('manager.task')->em()->persist($data);
        $this->get('manager.task')->em()->flush();

        return $this->successResponse([
            'task' => $data,
            'taskUrl' => $this->generateUrl('app_task_view', ['slug' => $data->getSlug()])
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $query = $request->query->get('q');

        if (!$query) {
            return $this->successResponse([]);
        }

        /** @var Task[] $tasks */
        $tasks = $this->get('manager.task')->repository()
            ->createQueryBuilder('t')
            ->andWhere('lower(t.title) like lower(:query)')
            ->setParameter('query', "%{$query}%")
            ->setMaxResults(5)
            ->orderBy('t.createdAt', 'DESC')
            ->getQuery()->execute();

        $res = [];
        foreach($tasks as $task) {
            $res[] = [
                'slug' => $task->getSlug(),
                'title' => $task->getSlug(),
                'description' => $task->getSlug(),
                'id' => $task->getId(),
            ];
        }

        return $this->successResponse($tasks);
    }
}
<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Csrf\Exception\TokenNotFoundException;

/**
 * Class AbstractController
 * @package AppBundle\Controller
 */
abstract class AbstractController extends Controller
{
    /**
     * Get a user from the Security Context
     *
     * @return mixed
     *
     * @throws \LogicException If SecurityBundle is not available
     * @throws TokenNotFoundException If token not found
     * @throws AuthenticationException If user is not authenticated
     *
     * @see Symfony\Component\Security\Core\Authentication\Token\TokenInterface::getUser()
     */
    public function getUser()
    {
        parent::getUser();

        /** @var $token TokenInterface */
        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            throw new TokenNotFoundException('Can not get token from storage.');
        }

        if (!is_object($user = $token->getUser())) {
            throw new AuthenticationException('User is not authenticated.');
        }

        return $user;
    }
}
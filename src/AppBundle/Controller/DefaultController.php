<?php

namespace AppBundle\Controller;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
//        $tok = $this->container->get('security.token_storage')->getToken();
//        $user = $tok->getUser();
//        $rem = $this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED');

        return $this->render('AppBundle:Default:index.html.twig');
    }
}

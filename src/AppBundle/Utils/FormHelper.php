<?php

namespace AppBundle\Utils;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

class FormHelper
{
    /**
     * @var TranslatorInterface
     */
    private $trans;

    /**
     * @param TranslatorInterface $trans
     */
    public function __construct(TranslatorInterface $trans)
    {
        $this->trans = $trans;
    }

    /**
     * Pass form object and call it to get resulting array.
     * @param FormInterface $form
     * @return array
     */
    public function getErrors(FormInterface $form)
    {
        $results = [];
        return $this->parseErrors($form, $results);
    }

    /**
     * @param FormInterface $form
     * @param bool $humanize if true prepend label to message
     * @return array
     */
    public function getErrorsMessages(FormInterface $form, $humanize = true)
    {
        $errors = $this->getErrors($form);
        $result = [];
        foreach($errors as $error) {
            $label = '';
            if ($error['label'] && $humanize) {
                $label = "{$error['label']}: ";
            }

            foreach($error['errors'] as $message) {
                array_push($result, sprintf('%s%s', $label, $message));
            }
        }

        return $result;
    }

    /**
     * Method travels through all levels of form recursively and gathers errors.
     * @param FormInterface $form
     * @param array &$results
     * @return array ['name' => ..., 'label' => ..., 'errors' => ...]
     */
    private function parseErrors(FormInterface $form, array &$results)
    {
        $errors = $form->getErrors();

        if(count($errors)){
            $config = $form->getConfig();
            $name = $form->getName();
            $label = $config->getOption('label');
            $domain = $config->getOption('translation_domain');
            $messages = [];
            foreach ($errors as $error) {
                array_push($messages, $error->getMessage());
            }
            $results[] = ['name' => $name, 'label' => $this->trans->trans($label, [], $domain), 'errors' => $messages];
        }

        $children = $form->all();
        if(count($children)){
            foreach($children as $child){
                if($child instanceof FormInterface){
                    $this->parseErrors($child, $results);
                }
            }
        }

        return $results;
    }
}
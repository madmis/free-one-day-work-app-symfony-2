<?php

namespace AppBundle\Utils;

use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class Pager
{
    /**
     * @param Query $query
     * @param int $currentPage
     * @param int $maxPerPage
     * @return Pagerfanta
     */
    public function create(Query $query, $currentPage, $maxPerPage)
    {
        $pager = new Pagerfanta(new DoctrineORMAdapter($query));
        $pager->setMaxPerPage($maxPerPage);
        $pager->setCurrentPage($currentPage);

        return $pager;
    }
}
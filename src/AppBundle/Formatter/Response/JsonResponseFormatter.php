<?php

namespace AppBundle\Formatter\Response;

/**
 * Class JsonResponseFormatter
 * @package Mds\ApiBundle\Formatter\Response
 */
class JsonResponseFormatter
{
    /**
     * Get security headers
     * @return array
     */
    public function securityHeaders()
    {
        return [
            'X-Frame-Options' => 'SAMEORIGIN',
            'X-XSS-Protection' => '1; mode=block',
            'Access-Control-Allow-Origin' => '*',
        ];
    }

    /**
     * @param array $messages messages array
     * @param int $httpCode http status code
     * @param array $params messages params
     * @return array
     */
    public function error(array $messages, $httpCode, array $params = [])
    {
        return [
            'error' => [
                'code' => $httpCode,
                'messages' => $messages,
                'params' => $params,
            ],
        ];
    }

    /**
     * @param array $data
     * @param array $messages
     * @param array $paging
     * @return array
     */
    public function success(array $data, array $messages = [], array $paging = [])
    {
        return [
            'data' => $data,
            'message' => $messages,
            'paging' => $paging,
        ];
    }
}
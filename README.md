One day work application
========================

What's inside?
--------------

  * PHP >= 5.6

  * Symfony2

  * PostgreSql >= 9.3

Installation
--------------
Clone project 
    
    git@bitbucket.org:madmis/free.git
    
Install packages
    
    composer install

Dev social auth configuration available here [https://bitbucket.org/madmis/free/wiki/Configuration][1]

Create database and after that run command (create schema)

    php app/console doctrine:schema:update --force

Fill default categories

    php app/console app:add-default-categories

Fill default locations

    php app/console app:parse-cities

Install assets

    php app/console assets:install --symlink
    
Set permissions to upload dir: [use acl][2]

[1]: https://bitbucket.org/madmis/free/wiki/Configuration
[2]: http://symfony.com/doc/current/book/installation.html#checking-symfony-application-configuration-and-setup

Nginx configuration
--------------

    server {
        listen 80;
     
        root /path/to/project/web/;
        server_name staffiz.dev staffiz.com;
     
        set $front_part app_dev;
        set $front $front_part.php;
     
        rewrite ^/$front_part\.php/?(.*)$ /$1 permanent;
        try_files $uri @rewriteapp;
     
        location @rewriteapp {
            rewrite ^(.*)$ /$front/$1 last;
        }
     
        location ~ ^/(app|app_dev|config)\.php(/|$) {
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_index $front;
            send_timeout 1800;
            fastcgi_read_timeout 1800;
            fastcgi_pass unix:/var/run/php5-fpm.sock;
        }
     
        # Statics
        location /(bundles|media) {
            access_log off;
            expires 30d;
     
            # Font files
            #if ($filename ~* ^.*?\.(eot)|(ttf)|(woff)$){
            #       add_header Access-Control-Allow-Origin *;
            #}
     
            try_files $uri @rewriteapp;
        }
     
        access_log /var/log/nginx/staffiz.dev-access.log;
        error_log /var/log/nginx/staffiz.dev-error.log;
     
        location = /favicon.ico { access_log off; log_not_found off; }
        location = /robots.txt { access_log off; log_not_found off; }
        location ~ /\. { deny all; access_log off; log_not_found off; }
    }
